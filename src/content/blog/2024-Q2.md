---
title: 2024 Q2
pubDatetime: 2024-07-01 02:22:51
postSlug: "2024-q2"
featured: false
draft: false
tags:
  - pawa
  - quarter
  - goals
description: A summary of the progress made in 2024 Q2 and 2024 Q3 goals.
---

And just like that, another quarter has passed! Here's the recap as to how the last quarter went, and,
right off the bat, having a predetermined set of goals make it working towards them much easier
than just working on what feels right.

For [2024 Q2](https://gitlab.com/pawabot/pawa/-/milestones/3#tab-issues) we worked on the following:

- `/info` command shows AutoSave status
- Add speakers to the ID3 metadata, and also it's part of the embed message
- Upgrade to Java 21 LTS

For full source changes go to [GitLab](https://gitlab.com/pawabot/pawa/-/compare/36cfb532...59cd9c44)
or visit the [CHANGELOG](https://pawa.im/#/changelog).

Let's dig into each one of them.

### Show AutoSave status

See this [post](autosave-info).

### Add ID3 metadata

This was a request from our [PawaLite](https://pawa.im/#/pawalite) users. They wanted to have a list
of speakers listed in the MP3's metadata.

![Speakers List in ID3 Metadata](@assets/images/recording-speakers-id3.png)

Since we already had the list of speakers to add them to the MP3 metadata, it was a simple stretch to
include this in the Discord message embed as well!

![Speakers List in Embed](@assets/images/recording-speakers-embed.png)

If you would like to have other metadata included here, we already have the pipeline in place so chat with
us on Discord!

### Upgrade to Java 21 LTS

This change did happen and it has little bearing to your experience using `pawa` on the day to day basis.
Keeping up to date with the latest Java version ensures that we're getting updates for security and performance
improvements, which will affect `pawa` indirectly.

### Stretch Goal: Show speakers on webapp

Failed! To make this change, it would require changes to our database and we also need a way to obtain the
user's pfps so instead we'll include this in a new Stretch Goal for Q3.

### Bonus: Google Analytics

_Sigh..._

We already track page metrics using [Plausible](https://plausible.io/), we do this to understand how much
traffic we're getting and if there's any errors that are happening that we can address before you all notice.

The reason for _also_ adding Google Analytics is because ad providers only accept GA as proof of traffic.

I want ads to be the last resort for monetization, but I need to start collecting metrics now if I want to
turn the ads machine on at some point. I'd much rather monetize in a way that provides value to you all so
I'm open to new ideas!

## Q3 Goals

For [Q3](https://gitlab.com/pawabot/pawa/-/milestones/4#tab-issues)'s milestone, I'd like to focus on addressing
performance and quality of life improvements. This is what I have in mind.

### `pawa`'s messages

Embed messages did not exist when we created `pawa`, so we send messages on channels to communicate. We added slash
commands and experimented with embed messages. They look sooooo much better!

The goal for this is simple, move communication to embed messages rather than direct messages to a channel. We'll start
with the `/recording` command. My vision is to only send one embed message and keep updating that.

### `/recover` command usefulness

I fixed some issues in the previous version of `pawa` that made the `recover` command not as useful. Most of the time it
is not able to find a recording but I need to know why. It's usually one of these three cases:

- The recording expired, it's been longer than 24hrs and we deleted it
- The recording is on Discord, we don't save the MP3 so we can simply return a link to the user.
- The recording was not saved, this is the most common case, user needs to explicitly save recording.

There's no features here, just quality of life improvements for better feedback. After this, I can probably make this
command available for others to use. Should this be a premium command?

### Database Query Performance

Not much here, I use [PgHero](https://github.com/ankane/pghero) to monitor the performance of the database queries
made, and it's currently flagging two potential improvements. I also noticed some areas of the code that are making
way more queries than they ought to be making slowing startup.

### Stretch Goal: Add Log in with Discord to the webapp

This ties to the last quarter's goal to show the speakers for the recording. What I'd like to do instead is enable the
login button and have a proper landing page that will allow you to choose the server that you want to look at. We'll
start with no customization only viewing the recordings that for that server.

### Stretch Goal: Upgrade PostgresQL 13.1 to 16

Paired with the above, the version of our database is comically old... There's a tiny risk in data loss because we have
hourly backups of the database. Restoring from backup will cause downtime, so let's hope it doesn't come to that.

### Stretch Goal: Migrate Server

We're hosting `pawa` in a really old version of CentOS and it's no longer supported. I've had another server ready
to go just need to install all the things. Might pair this task with the database upgrade.

Been procrastinating this change because it'll cause downtime and can be potentially time consuming.
