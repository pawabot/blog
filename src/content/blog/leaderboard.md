---
title: Recordings Leaderboard
pubDatetime: 2023-05-29 01:43:25-07:00
postSlug: leaderboard
featured: false
draft: false
tags:
  - pawa
  - feature
description: Leaderboard showing the most active servers.
---

We're doing some changes to the pawa's database to improve its performance, and while doing that, I got curious and wondered
what was the highest number of recordings over the last week. Which led me to the idea of creating a public leaderboard
showing exactly that!

Check out the public [leaderboard](https://app.pawa.im/v1/leaderboard) on the app's website.

![leaderboard](@assets/images/leaderboard.png)
