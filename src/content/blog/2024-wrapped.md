---
title: 2024 Year in Review
pubDatetime: 2025-02-08
postSlug: "2024-year-in-review"
featured: true
draft: false
tags:
  - pawa
  - wrapped
  - stats
description: A summary of the stats for the year 2024.
---

Woof! Took me a while to get this post out! We're already in February but still wanted create a 2024 wrap up post. Let's get into it!

## Highlights

We had several goals in 2024 for `@pawa` but we did not accomplish all of them. Here are some of the notable highlights:

- `/recover` command is more [useful](https://gitlab.com/pawabot/pawa/-/merge_requests/108), provides context as to why we could or could not recover
- The Recording embed now has information about expected expiration and who were the members that were part of the recording.
- Add `/save-destination` command, save the recordings in a specific channel, similar to `!saveDestination`
- Migrated to new faster server
  - We've had this server for years but never did the migration.
- Upgraded to the latest version of PostgresQL
  - This was on my list for a long time!
- Three major releases and one minor, shy of the goal of one per quarter.

## Reflections

### Monetization

> If this doesn’t pan out, 2024 will be the last year of this project.

This year we made _just_ enough to cover the server costs. We made this money through the sales of [PawaLite](https://ko-fi.com/s/f6fde9fd95), if you want to support this project check it out.

It is clear that we need to be more serious about this goal if we want this project to continue living.

### Stability

Moving to the new server improved stability. We haven't seen cases where `@pawa` straight up refuses to restart on it's own. _However_, there has been an influx on recovery requests. The `/recover` command is a stopgap solution until we find the real problem. Need to figure that out.

### Marketing

We created 0 videos this year on YouTube. There's a lot of commands that don't have a video, and we should do one for new features as we create them. Furthermore, the PawaLite onboarding video needs a revamp.

## Goals for 2025

Will keep this simple as last year we were super ambitious:

- Reduce the number of recoveries needed
  - Will need to track the number of failures
  - Will also help with a bug in PawaLite where it stops recording without alerting
- Publish one video per quarter

## Year in Review

Here are the stats for the year!

- The week with the most recordings was **August 4th** with **3,820** recordings
- There were **140.9k recordings** in total
- Which amounted to **1,431 GB** in storage
- This adds up to **3.19 years** in recorded time
- As of today, there are **10,875** active servers
- Most recordings are less than **23 MB**
- There were **66.5k unique visitors** to https://app.pawa.im
- The week with the most views was **February 12th** with **2.8k views**

### Stats

![2024 Stats](@assets/images/2024-stats.png "Stats")

### Page Views

![2024 Pageviews](@assets/images/2024-pageviews.png "Pageviews")

### [YouTube](https://www.youtube.com/channel/UCSCFEcisMTHnJ4SdO09y9GQ) Views

![2024 YouTube Views](@assets/images/2024-youtube-views.png "YouTube Views")
