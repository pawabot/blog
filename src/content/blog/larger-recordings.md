---
title: Increase Recording Limit 110MB -> 256MB
pubDatetime: 2023-06-10 09:58:04-07:00
postSlug: larger-recordings
featured: false
draft: false
tags:
  - pawa
  - feature
description: The recording limit is now 256MB up from 110MB.
---

We've been making some updates to the server hosting pawa, and realized that we had some extra headroom. So we've decided to increase the recording size limit to allow for longer uninterrupted sessions.

This change went live on May 23rd, 2023 and we can already see that some of you have been taking advantage this! Check out the recording size distribution for the past 30 days.

![Recording size distribution](@assets/images/recording-size-distribution-jun-may-2023.png)
