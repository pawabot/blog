---
title: 2023 Year in Review
pubDatetime: 2024-01-04
postSlug: "2023-year-in-review"
featured: false
draft: false
tags:
  - pawa
  - wrapped
  - stats
description: A summary of the stats for the year 2023.
---

Another year for `@pawa` on the books! We're reaching a point where `@pawa` is mostly stable and doesn't require more feature additions. Most of the changes this year happened behind the scenes, here are some highlights:

- Increased max recording size from 110MB to 256MB
- Add internal `/recover` command
  - This let's me recover a recording without having to be at my computer
- Started tracking events on https://app.pawa.im
  - This helps me see any issues and the number of visits the page is getting
  - Planning on publishing the stats publicly this year

## Goals for 2024

I don't personally have any resolutions for this upcoming year, but for `@pawa` these are the goals I have in mind.

- Update PawaLite's onboarding documentation and get it out of alpha stage
  - Thank you to all of you who are choosing to support the early development of this effort!
- Drop prefix commands to `@pawa`, those have always been a hack and slash commands are better for everyone
- Localize slash commands, Discord supports this natively now.
- More releases! Goal one per quarter.
- Monetize...
  - Yes it's come to that, my goal is to have `@pawa` pay for itself in terms of hosting and effort. I've been hosting for years and have made only enough to cover the hosting costs through PawaLite. So this may be either in the form of subscriptions or ads.
    If this doesn't pan out, 2024 will be the last year of this project.

## Year in Review

Here are the stats for the year!

- The week with the most recordings was **January 1st** with **3,041** recordings
- There were **128.5k recordings** in total
- Which amounted to **1,555 GB** in storage
- This adds up to **3.47 years** in recorded time
- As of today, there are **6,853** active servers
- Most recordings are less than **23 MB**
- There were **54.9k unique visitors** to https://app.pawa.im
- The week with the most views was **September 4th** with **2.9k views**

### Stats

![2023 Stats](@assets/images/2023-stats.png "Stats")

### Page Views

![2023 Pageviews](@assets/images/2023-pageviews.png "Pageviews")

### [YouTube](https://www.youtube.com/channel/UCSCFEcisMTHnJ4SdO09y9GQ) Views

![2023 YouTube Views](@assets/images/2023-youtube-views.png "YouTube Views")
