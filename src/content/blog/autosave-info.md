---
title: Autosave Status
pubDatetime: 2024-06-10
postSlug: "autosave-info"
featured: false
draft: false
tags:
  - pawa
  - command
description: "`/info` command now shows `autosave` status"
---

One of the main requests in the support server is to recover a recording that wasn't saved. By design, `pawa` will
delete any recording that isn't _explicitly_ saved using the [`save`](https://pawa.im/#/commands/slash/save) command.

We do these reasons:

- The process of turning the temporary file into an audio file is computationally expensive
- The user doesn't always care about the recording and creates waste in terms of uploads and processing
- If we exceed thresholds, the above two actions costs us money $$$

Yet, we provide a way for users to eat their cake and have it too via the [`autosave`](https://pawa.im/#/commands/slash/autosave)
command. When enabled, `pawa` will save _all_ recordings automatically. Making this an opt-in feature
mitigates the problem we had above.

The problem is that there's no easy way for users to know if the feature is enabled, but now you can do that with `/info`!

![/info screenshot](@assets/images/autosave-info.png)

Enjoy!
